require 'capybara/dsl'

Capybara.register_driver :selenium do |app|
  Capybara::Selenium::Driver.new(app, :browser => :chrome)
end
Capybara.current_driver = :selenium

include Capybara::DSL

puts "started"
visit "http://bit.ly/2nIgMW9"
puts "visited"
1601.times do |i|
  print "."
  puts("100 secs complete" + find(".well h3:nth-child(4)").text()) if i % 100 == 0 && i != 0
  sleep 1
end